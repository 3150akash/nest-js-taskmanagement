import { Injectable, NotFoundException } from '@nestjs/common';
import { TaskStatus } from './task-status.enum';
import { CreateTaskDto } from './dto/create-task-dto';
import { GetTasksFilterDto } from './dto/get-tasks-filter-dto';
import { InjectRepository } from '@nestjs/typeorm';
import { TaskRepository } from './task.repository';
import { Task } from './task.entity';

@Injectable()
export class TasksService {
  constructor(
    @InjectRepository(TaskRepository)
    private taskRepository: TaskRepository,
  ) {}

  public getTasks = async (filterDto: GetTasksFilterDto): Promise<Task[]> => {
    return this.taskRepository.getTasks(filterDto);
  };
  // private tasks: Task[] = [];
  // public getAllTasks = (): Task[] => {
  //   return this.tasks;
  // };
  // public getTaskWithFilters = (filterDto: GetTasksFilterDto): Task[] => {
  //   const { status, search } = filterDto;
  //   let tasks = this.getAllTasks();
  //   if (status) {
  //     tasks = tasks.filter(task => task.status === status);
  //   }
  //   if (search) {
  //     tasks = tasks.filter(
  //       task =>
  //         task.title.includes(search) || task.description.includes(search),
  //     );
  //   }
  //   return tasks;
  // };
  public getTaskById = async (id: number): Promise<Task> => {
    const found = await this.taskRepository.findOne(id);
    if (!found) {
      throw new NotFoundException(`Task with Id ${id} not found`);
      // throw new NotFoundException();
    }
    return found;
  };

  public deleteTaskById = async (id: number): Promise<string> => {
    const deleteResult = await this.taskRepository.delete(id);
    if (deleteResult.affected === 0) {
      throw new NotFoundException(`Task with Id ${id} not found`);
    }
    return `item with id ${id} deleted successfully`;
  };

  public updateTaskById = async (
    id: number,
    status: TaskStatus,
  ): Promise<Task> => {
    const task = await this.getTaskById(id);
    task.status = status;
    task.save();
    return task;
  };

  public createTasks = (createTaskDto: CreateTaskDto): Promise<Task> => {
    return this.taskRepository.createTask(createTaskDto);
  };
}
